import { Module, Scope } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { validationSchemas } from '../../../package';

@Module({
  imports: [
    ConfigModule.forRoot({ validationSchema: validationSchemas.env() }),
  ],
})
export class AppModule {}
