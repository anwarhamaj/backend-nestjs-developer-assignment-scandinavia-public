import { Module, Scope } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongoDbConnection, validationSchemas } from '../../../package';
import Modules from './app';

@Module({
  imports: [MongoDbConnection, ...Modules],
  providers: [],
})
export class AppModule {}
