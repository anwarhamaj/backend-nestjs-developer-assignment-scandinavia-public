import * as dotenv from 'dotenv';
dotenv.config();

// Server Config
export const localServerConfig = {
  port: process.env.EVENT_API_SERVER_PORT,
};
