import { Event, EventDocument, EventSchema } from './data/event.schema';
import { EventReceiverApiApiService } from './services';
import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EventReceiverApiController } from './api/controllers';
import { EventReceiverApiError } from './services/eventReceiverApi.error';
import { EventReceiverApiValidation } from './api/validation';
import { EventRepository } from './data/event.repository';

@Module({
  controllers: [EventReceiverApiController],
  providers: [
    EventReceiverApiError,
    EventReceiverApiValidation,
    EventReceiverApiApiService,
    EventRepository,
  ],
  imports: [
    MongooseModule.forFeature([{ name: Event.name, schema: EventSchema }]),
  ],
  exports: [EventReceiverApiApiService],
})
export class EventReceiverApiModule {}
