import { Injectable } from '@nestjs/common';
import { IError } from '../../../../../../package';

@Injectable()
export class EventReceiverApiError {
  notFoundError: IError = {
    code: 8585,
    message: 'Event not found',
  };

  notFound() {
    return this.notFoundError;
  }
}
