import { GetByCriteriaResponse } from '../api/dto';
import {
  IPagination,
  IParamsId,
  getSortFormat,
} from '../../../../../../package';

import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { EventReceiverApiError } from './eventReceiverApi.error';
import mongoose from 'mongoose';
import { InjectConnection } from '@nestjs/mongoose';
import { GetByIdEventReceiverApiResponse } from '../api/dto/response/get-by-id-response.dto';
import { EventRepository } from '../data/event.repository';
import { EventDocument } from '../data/event.schema';

@Injectable()
export class EventReceiverApiApiService {
  constructor(
    private readonly eventRepository: EventRepository,
    private readonly eventReceiverApiError: EventReceiverApiError,
  ) {}

  //get all eventReceiverApi
  async findAll(
    criteria: { sort: string },
    { limit, skip, total, needPagination }: IPagination,
  ) {
    const options: {
      limit?: number;
      skip?: number;
      populate?: { model: any; path: string }[];
      sort: { [key: string]: number };
    } = {
      sort: getSortFormat(criteria.sort),
    };

    if (needPagination) {
      options.limit = limit;
      options.skip = skip;
    }

    const projection = '_id name description';

    const queries: Promise<any>[] = [];

    queries.push(
      this.eventRepository.find({
        projection,
        options,
      }),
    );
    if (total) {
      queries.push(this.eventRepository.countDocuments({}));
    }

    const [events, totalRecords = undefined] = await Promise.all(queries);

    return {
      totalRecords,
      data: events.map(
        (event: EventDocument) =>
          new GetByCriteriaResponse({
            event,
          }),
      ),
    };
  }

  async findOne({ id }: IParamsId) {
    const event = await this.eventRepository.findOne({
      filter: {
        _id: id,
      },
      error: this.eventReceiverApiError.notFound(),
    });

    return {
      data: new GetByIdEventReceiverApiResponse({
        event,
      }),
    };
  }
}
