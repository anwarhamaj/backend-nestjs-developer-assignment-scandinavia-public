import { EventDocument } from '../../../data/event.schema';

export class GetByCriteriaResponse {
  id: string;
  name: string;
  description: any;

  constructor({ event }: { event: EventDocument }) {
    this.id = event._id;
    this.name = event.name;
    this.description = event.description;
  }
}
