import { PaginationRequest } from '../../../../../../../package';

export class GetByCriteriaEventReceiverApiRequest extends PaginationRequest {
  sort: string;
}
