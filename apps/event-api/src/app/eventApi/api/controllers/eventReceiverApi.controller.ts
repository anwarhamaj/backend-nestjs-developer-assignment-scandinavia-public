import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';

import {
  IPagination,
  IParamsId,
  ResponseWrapper,
  paginationParser,
} from '../../../../../../../package';

import { EventReceiverApiApiService } from '../../services';
import { GetByCriteriaEventReceiverApiRequest } from '../dto';
import { EventReceiverApiValidation } from '../validation';

@Controller('eventReceiverApi')
export class EventReceiverApiController {
  constructor(
    private readonly eventReceiverApiService: EventReceiverApiApiService,
    private readonly eventReceiverApiValidation: EventReceiverApiValidation,
  ) {}
  //######################### Get By Id Api #########################
  @Get(':id')
  async findOne(@Param() paramsId: IParamsId) {
    this.eventReceiverApiValidation.paramsId({ paramsId });
    const eventReceiverApi = await this.eventReceiverApiService.findOne(
      paramsId,
    );
    return new ResponseWrapper(eventReceiverApi);
  }

  //######################### Get By Criteria  Api #########################
  @Get('')
  async findAll(@Query() query: GetByCriteriaEventReceiverApiRequest) {
    this.eventReceiverApiValidation.getByCriteria({
      query,
      needPagination: [true, false],
    });
    const { pagination, criteria } = paginationParser(query) as {
      pagination: IPagination;
      criteria: { sort: string };
    };
    const EventReceiverApis = await this.eventReceiverApiService.findAll(
      criteria,
      pagination,
    );
    return new ResponseWrapper(EventReceiverApis);
  }
}
