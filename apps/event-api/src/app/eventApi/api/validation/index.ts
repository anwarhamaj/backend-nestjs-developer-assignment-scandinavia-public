import { GetByCriteriaEventReceiverApiRequest } from '../dto';
import * as joi from 'joi';

import {
  IParamsId,
  JoiValidationPipe,
  joiPagination,
  validationSchemas,
} from '../../../../../../../package';

import { Injectable } from '@nestjs/common';

@Injectable()
export class EventReceiverApiValidation {
  getByCriteria({
    query,
    needPagination,
  }: {
    query: GetByCriteriaEventReceiverApiRequest;
    needPagination: [boolean, ...boolean[]];
  }) {
    const getByCriteria = joi.object({
      ...joiPagination(needPagination),
      sort: joi.string().valid('createdAt', '-createdAt').required(),
    });
    return new JoiValidationPipe(getByCriteria).transform(query);
  }

  paramsId({ paramsId }: { paramsId: IParamsId }) {
    const id = joi.object({ id: validationSchemas.mongoId().required() });
    return new JoiValidationPipe(id).transform(paramsId);
  }
}
