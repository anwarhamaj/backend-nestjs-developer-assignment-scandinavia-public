import { Prop, SchemaFactory, Schema } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

export type EventDocument = Event & Document;
@Schema({ timestamps: true })
export class Event {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: String, required: true })
  description: string;
}

export const EventSchema = SchemaFactory.createForClass(Event);
