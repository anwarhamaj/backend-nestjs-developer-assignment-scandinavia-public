import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { EventDocument, Event } from './event.schema';
import { BaseMongoRepository } from '../../../../../../package';

@Injectable()
export class EventRepository extends BaseMongoRepository<Event, EventDocument> {
  constructor(
    @InjectModel(Event.name)
    eventReceiverModel: Model<EventDocument>,
  ) {
    super(eventReceiverModel);
  }
}
