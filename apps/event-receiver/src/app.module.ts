import { Module, Scope } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongoDbConnection, validationSchemas } from '../../../package';
import Modules from './app';
@Module({
  imports: [
    ConfigModule.forRoot({ validationSchema: validationSchemas.env() }),
    MongoDbConnection,
    ...Modules,
  ],
})
export class AppModule {}
