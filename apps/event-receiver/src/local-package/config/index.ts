import * as dotenv from 'dotenv';
dotenv.config();

// Server Config
export const localServerConfig = {
  // server data
  rabbitMqUri: process.env.RABBIT_MQ_URI,
  port: process.env.EVENT_RECEIVER_SERVER_PORT,
};
