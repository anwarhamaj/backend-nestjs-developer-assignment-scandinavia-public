import { Injectable } from '@nestjs/common';
import { EventRepository } from '../data/event.repository';
import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';

@Injectable()
export class EventReceiverService {
  constructor(private readonly eventRepository: EventRepository) {}

  @RabbitSubscribe({
    exchange: 'eventsExchange',
    routingKey: 'eventRoutingKey',
    queue: 'eventsQueue',
  })
  public async create(event: { name: string; description: string }) {
    await this.eventRepository.create({ doc: event });
  }
}
