import { EventSchema, Event } from './data/event.schema';
import { EventReceiverService } from './services';
import { rabbitMqConnection } from '../../../../../package';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EventRepository } from './data/event.repository';
import { localServerConfig } from '../../local-package';

@Module({
  providers: [EventRepository, EventReceiverService],
  imports: [
    rabbitMqConnection({
      exchanges: [{ name: 'eventsExchange', type: 'direct' }],
      uri: localServerConfig.rabbitMqUri,
    }),
    MongooseModule.forFeature([{ name: Event.name, schema: EventSchema }]),
  ],
  exports: [EventReceiverService],
})
export class EventReceiverModule {}
