import * as joi from 'joi';

export const env = () => {
  return joi.object({
    // RABBIT_Mq_URI
    RABBIT_MQ_URI: joi.string().required(),
  });
};
