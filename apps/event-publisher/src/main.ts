process.env.TZ = 'UTC';
import { NestFactory } from '@nestjs/core';
import { VersioningType } from '@nestjs/common';
import { AppModule } from './app.module';
import { serverConfig } from '../../../package';
import { localServerConfig } from './local-package';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });

  app.enableCors();
  app.setGlobalPrefix(serverConfig.globalPrefix);

  app.enableVersioning({
    defaultVersion: [serverConfig.apIVersion],
    type: VersioningType.URI,
  });

  await app.listen(localServerConfig.port, () => {
    console.log(
      `Server Running on http://${serverConfig.host}:${localServerConfig.port}\n `,
    );
  });
}

bootstrap();
