import { Module } from '@nestjs/common';
import { RabbitMqService, rabbitMqConnection } from '../../../../../package';
import { localServerConfig } from '../../local-package';
import { EventPublisherService } from './eventPublisher.service';

@Module({
  imports: [
    rabbitMqConnection({
      exchanges: [
        {
          name: 'exchange1',
          type: 'topic',
        },
      ],
      uri: localServerConfig.rabbitMqUri,
    }),
  ],
  providers: [EventPublisherService, RabbitMqService],
})
export class EventPublisherModule {}
