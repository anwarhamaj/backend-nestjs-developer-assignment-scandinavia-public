import { Injectable, OnModuleInit } from '@nestjs/common';
import { RabbitMqService } from 'package';

@Injectable()
export class EventPublisherService implements OnModuleInit {
  constructor(private readonly rabbitMqService: RabbitMqService) {}

  onModuleInit() {
    setInterval(() => {
      const event = { name: 'name', description: 'description' };
      this.rabbitMqService.publish<{ name: string; description: string }>(
        event,
      );
      console.log(
        '🚀 ~ file: app.service.ts:14 ~ AppService ~ setInterval ~ event:',
        event,
      );
    }, 1000);
  }
}
