import { Module } from '@nestjs/common';
import Modules from './app';

@Module({
  imports: [...Modules],
})
export class AppModule {}
