import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { MongoServerError } from 'mongodb';
import { Logger } from '@nestjs/common';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  private readonly logger = new Logger();

  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    switch (exception.constructor) {
      case HttpException: {
        // eslint-disable-next-line prefer-const
        let httpException = exception as HttpException;
        const status = httpException.getStatus();
        const { error } = httpException.getResponse() as { error: object };
        const res = {
          isCustom: true,
          statusCode: status,
          error,
          timestamp: new Date().toISOString(),
          path: request.url,
        };
        this.logger.error({ ...res, message: httpException.getResponse() });
        response.status(status).json(res);
        break;
      }
      case MongoServerError: {
        // eslint-disable-next-line prefer-const
        let mongoException = exception as MongoServerError;
        switch (mongoException.code) {
          case 11000: {
            const error = {
              code:
                mongoException.code + `${Object.keys(mongoException.keyValue)}`,
              message: `Duplicate unique key '${Object.keys(
                mongoException.keyValue,
              )}'`,
            };
            const res = {
              isCustom: true,
              statusCode: HttpStatus.BAD_REQUEST,
              error,
              timestamp: new Date().toISOString(),
              path: request.url,
            };
            this.logger.error({ ...res, message: error });
            response.status(HttpStatus.BAD_REQUEST).json(res);
            break;
          }
          default:
            // console.log(exception);
            this.logger.error(exception);
            response.status(500).json({
              isCustom: false,
              statusCode: 500,
              error: {
                code: 50003,
                message:
                  'Failed to do something async with an unspecified mongo error',
              },
              exception,
              timestamp: new Date().toISOString(),
              path: request.url,
            });
        }
        break;
      }
      default: {
        switch (true) {
          case exception.constructor
            ?.toString()
            .includes('class MongoServerError extends MongoError'): {
            const mongoException = exception as MongoServerError;
            console.log(mongoException);
            const error = {
              code:
                mongoException.code + `${Object.keys(mongoException.keyValue)}`,
              message: `Duplicate unique key '${Object.keys(
                mongoException.keyValue,
              )}'`,
            };
            const res = {
              isCustom: true,
              statusCode: HttpStatus.BAD_REQUEST,
              error,
              timestamp: new Date().toISOString(),
              path: request.url,
            };
            this.logger.error({ ...res, message: error });
            response.status(HttpStatus.BAD_REQUEST).json(res);
            break;
          }
          default: {
            this.logger.error(exception);
            const { message } = exception as { message: string };
            response.status(500).json({
              isCustom: false,
              statusCode: 500,
              error: {
                code: 500004,
                message,
              },
              timestamp: new Date().toISOString(),
              path: request.url,
            });
          }
        }
      }
    }
  }
}
