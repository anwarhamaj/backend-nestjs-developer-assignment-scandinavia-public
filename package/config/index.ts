import * as dotenv from 'dotenv';
dotenv.config();

// Server Config
export const serverConfig = {
  // server data
  host: process.env.SERVER_HOST,
  port: process.env.SERVER_PORT,

  //node environment
  nodeEnv: process.env.NODE_ENV,

  // api config
  apIVersion: process.env.API_VERSION,
  globalPrefix: process.env.GLOBAL_PREFIX,

  //swagger config
  swaggerUsername: process.env.SWAGGER_USERNAME,
  swaggerPassword: process.env.SWAGGER_PASSWORD,

  // default keys use for joi validation and database validation
  defaultLanguage: process.env.DEFAULT_LANGUAGE,

  //file config
  mainLocalFileUploadDestination:
    process.env.MAIN_LOCAL_FILE_UPLOAD_DESTINATION,
  storageUploadLostFile: process.env.STORAGE_UPLOADED_LOST_FILES,
  fileUrlPrefix: process.env.FILE_URL_PREFIX,
  fileSaveBasePath: process.env.FILE_SAVE_BASE_PATH,

  //server url
  serverUrl: process.env.SERVER_URL,
};
