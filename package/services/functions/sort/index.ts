export const getSortFormat = (sortKey: string) =>
  sortKey.startsWith('-')
    ? { [`${sortKey.substring(1)}`]: -1 }
    : { [`${sortKey}`]: 1 };
