import { RabbitMQConfig, RabbitMQModule } from '@golevelup/nestjs-rabbitmq';

export const rabbitMqConnection = ({
  exchanges,
  uri,
}: {
  exchanges: { name: string; type: string }[];
  uri: string;
}) =>
  RabbitMQModule.forRoot(RabbitMQModule, {
    exchanges,
    uri,
    connectionInitOptions: { wait: true },
    
  });
