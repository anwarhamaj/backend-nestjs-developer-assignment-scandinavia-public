import { Injectable } from '@nestjs/common';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Channel } from 'amqp-connection-manager';

@Injectable()
export class RabbitMqService {
  private channel: Channel;

  constructor(private readonly amqpConnection: AmqpConnection) {
    this.channel = this.amqpConnection.channel;
    this.initializeExchange();
  }

  private async initializeExchange() {
    const exchangeName = 'eventsExchange';
    const exchangeType = 'direct';
    const durable = true;
    await this.channel.assertExchange(exchangeName, exchangeType, { durable });
  }

  async publish<MessageType>(message: MessageType) {
    this.amqpConnection.publish('eventsExchange', 'eventRoutingKey', message);
  }
}
