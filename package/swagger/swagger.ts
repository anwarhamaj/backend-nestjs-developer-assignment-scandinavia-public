import { INestApplication } from '@nestjs/common';
import {
  DocumentBuilder,
  OpenAPIObject,
  SwaggerCustomOptions,
  SwaggerModule,
} from '@nestjs/swagger';

export default (
  title: string,
  desc: string,
  version: string,
  app: INestApplication,
  modules: any[],
) =>
  SwaggerModule.createDocument(
    app,
    new DocumentBuilder()
      .setTitle(title)
      .setDescription(desc)
      .setVersion(version)
      .addBearerAuth()
      .build(),
    {
      include: modules,
    },
  );

export const initSwagger = (
  url: string,
  app: INestApplication,
  docs: any,
  options?: SwaggerCustomOptions,
) => SwaggerModule.setup(url, app, docs, options);
