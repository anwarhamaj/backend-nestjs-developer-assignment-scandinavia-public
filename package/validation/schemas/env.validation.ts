import * as joi from 'joi';

export const env = () => {
  return joi.object({
    // server

    SERVER_PORT: joi.number().default('8000').required(),
    EVENT_RECEIVER_SERVER_PORT: joi.number().default('8001').required(),
    EVENT_API_SERVER_PORT: joi.number().default('8002').required(),
    EVENT_PUBLISHER_SERVER_PORT: joi.number().default('8003').required(),
    SERVER_HOST: joi.string().default('localhost').required(),
    API_VERSION: joi.number().default('1').required(),
    NODE_ENV: joi
      .string()
      .required()
      .valid('dev', 'production', 'testing')
      .default('dev')
      .required(),
    GLOBAL_PREFIX: joi.string().required(),

    //MONGO DB
    DB_URL: joi.string().required(),
    DB_NAME: joi.string().required(),
    DB_HOST: joi.string().default('localhost').required(),
    DB_PORT: joi.number().required(),
    DB_USER: joi.string().allow('', null),
    DB_USER_PASSWORD: joi.string().allow('', null),
  });
};
