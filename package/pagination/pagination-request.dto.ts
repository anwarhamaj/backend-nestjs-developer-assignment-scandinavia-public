export class PaginationRequest {
  total: boolean;

  needPagination: boolean;

  page: number;

  limit: number;
}
