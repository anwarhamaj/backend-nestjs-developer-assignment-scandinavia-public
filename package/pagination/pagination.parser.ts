const _ = require('lodash');
import { paginationConstant } from './pagination.constant';

export const paginationParser = (reqQuery) => {
  const fields = ['total', 'page', 'limit', 'needPagination'];

  const pagination = _.pick(reqQuery, fields);

  const page = +pagination.page || 0;

  pagination.total =
    pagination.total === 'true' ? true : paginationConstant.total;

  pagination.limit = +pagination.limit || paginationConstant.limit.default;

  pagination.skip = page * pagination.limit;

  pagination.needPagination =
    pagination.needPagination === 'false'
      ? false
      : paginationConstant.needPagination;

  const criteria = _.omit(reqQuery, fields);
  return { pagination, criteria };
};
