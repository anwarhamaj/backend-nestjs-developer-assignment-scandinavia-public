//pagination type
export interface IPagination {
  limit: number;
  skip: number;
  total: boolean;
  needPagination?: boolean;
}
