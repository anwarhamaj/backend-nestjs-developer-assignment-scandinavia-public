import { MongooseModule } from '@nestjs/mongoose';
import { mongoDbConfig } from './db.config';

const dbUrl =
  mongoDbConfig.dbUser && mongoDbConfig.dbPassword
    ? `mongodb://${mongoDbConfig.dbUser}:${mongoDbConfig.dbPassword}@${mongoDbConfig.dbHost}:${mongoDbConfig.dbPort}/${mongoDbConfig.dbName}`
    : `${mongoDbConfig.dbUrl}${mongoDbConfig.dbName}`;

export default MongooseModule.forRoot(dbUrl, {
  dbName: mongoDbConfig.dbName,
  bufferCommands: false,
  //useCreateIndex: true,
  // useFindAndModify: false,
  // useUnifiedTopology: true,
  retryWrites: false,
  connectionFactory: (connection) => {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    connection.plugin(require('mongoose-autopopulate'));
    connection.plugin((schema: any) => {
      schema.options.toJSON = {
        virtuals: true,
        versionKey: false,
        transform(doc: any, ret: any) {
          if (ret.password) delete ret.password;
          ret.id = ret._id;
          delete ret._id;
        },
      };
      schema.options.toObject = {
        virtuals: true,
        versionKey: false,
        transform(doc: any, ret: any) {
          if (ret.password) delete ret.password;
          delete ret._id;
        },
      };
    });
    return connection;
  },
});

export const mongoDbUrl = mongoDbConfig.dbUrl;
