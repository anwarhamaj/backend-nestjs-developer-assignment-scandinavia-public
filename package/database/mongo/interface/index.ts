import * as mongoose from 'mongoose';

import { Document } from 'mongoose';
import { IError } from '../../../types';

export interface BaseMongoInterface<V, T> {
  create({ doc, options }: { doc: V; options?: mongoose.SaveOptions });

  find({
    filter,
    projection,
    options,
  }: {
    filter?: mongoose.FilterQuery<T>;
    projection?: mongoose.ProjectionType<T>;
    options?: mongoose.QueryOptions<T>;
  });

  findOne({
    filter,
    projection,
    options,
    error,
  }: {
    filter?: mongoose.FilterQuery<T>;
    projection?: mongoose.ProjectionType<T>;
    options?: mongoose.QueryOptions<T>;
    error: IError;
  });

  findById({
    _id,
    projection,
    options,
    error,
  }: {
    _id: any;
    projection?: mongoose.ProjectionType<T>;
    options?: mongoose.QueryOptions<T>;
    error: IError;
  });
}
