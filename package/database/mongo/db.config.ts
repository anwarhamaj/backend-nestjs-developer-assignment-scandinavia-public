import * as dotenv from 'dotenv';
dotenv.config();

// DB Config
export const mongoDbConfig = {
  dbUrl: process.env.DB_URL,
  dbName: process.env.DB_NAME,
  dbHost: process.env.DB_HOST,
  dbPort: process.env.DB_PORT,
  dbUser: process.env.DB_USER,
  dbPassword: process.env.DB_USER_PASSWORD,
};
