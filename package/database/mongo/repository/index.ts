import * as mongoose from 'mongoose';

import { HttpException, HttpStatus } from '@nestjs/common';

import { BaseMongoInterface } from '../interface';
import { Document } from 'mongoose';
import { IError } from '../../../types';
import { boolean } from 'joi';

export abstract class BaseMongoRepository<V, T extends Document>
  implements BaseMongoInterface<V, T>
{
  constructor(private readonly entityModel: mongoose.Model<T>) {}
  async create({
    doc,
    options,
    needResult = false,
  }: {
    doc: V;
    options?: mongoose.SaveOptions;
    needResult?: boolean;
  }): Promise<mongoose.Require_id<T>['_id']> {
    const result = await new this.entityModel(doc).save(options);
    return needResult ? result : result._id;
  }

  async countDocuments({
    filter,
    options,
  }: {
    filter?: mongoose.FilterQuery<T>;
    options?: mongoose.QueryOptions<T>;
  }) {
    return await this.entityModel.countDocuments(filter, options);
  }

  async find({
    filter,
    projection,
    options,
  }: {
    filter?: mongoose.FilterQuery<T>;
    projection?: mongoose.ProjectionType<T>;
    options?: mongoose.QueryOptions<T>;
  }) {
    return await this.entityModel.find(filter, projection, {
      ...options,
      lean: true,
    });
  }

  async findOne({
    filter,
    projection,
    options,
    error,
    throwError = true,
  }: {
    filter?: mongoose.FilterQuery<T>;
    projection?: mongoose.ProjectionType<T>;
    options?: mongoose.QueryOptions<T>;
    error: IError;
    throwError?: boolean;
  }) {
    const result = await this.entityModel.findOne(filter, projection, {
      ...options,
      lean: true,
    });
    if (!result && throwError) {
      throw new HttpException({ error }, HttpStatus.NOT_FOUND);
    }
    return result;
  }

  async findById({
    _id,
    projection,
    options,
    error,
  }: {
    _id: string | mongoose.Types.ObjectId;
    projection?: mongoose.ProjectionType<T>;
    options?: mongoose.QueryOptions<T>;
    error: IError;
  }) {
    const result = await this.entityModel.findById(_id, projection, {
      ...options,
      lean: true,
    });
    if (!result) {
      throw new HttpException({ error }, HttpStatus.NOT_FOUND);
    }
    return result;
  }
}
