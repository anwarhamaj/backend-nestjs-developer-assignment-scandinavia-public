# Backend NestJS Developer Assignment - Scandinavia

## Description

This project is a NestJS-based backend application

## Prerequisites

- Node.js (v18.12.1)
- npm (Node Package Manager)

## Environment Configuration

Ensure to set up your `.env` file or environment variables as required by the project.

## Notes

- Ensure that any external services like databases, RabbitMQ, etc., are properly configured and running.

## Installation

1. Install [concurrently] as global package

```bash
npm i -g concurrently
```

2. Install all packages

```bash
npm i
```

## Running the Application

- **For Development:**

```bash
npm run start:dev
```

## Check API

- **For Post Man:**

```bash
path  : postnam\Test.postman_collection.json
```
